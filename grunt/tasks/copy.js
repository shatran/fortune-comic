module.exports = function() {
  this.loadNpmTasks("grunt-contrib-copy");

  // Move files during a build.
  this.config("copy", {
    dev: {
      files: [
        {src: 'public/index.html', dest: 'build/index.html'},
        {
          expand: true,
          cwd: '<%= config.public %>/images/',
          src: ['**/*.{png,jpg,svg,ico}'],
          dest: '<%= config.build %>/images/',
          filter: 'isFile'
        }
      ]
    },
    dist: {
      files: [
        {src: '<%= config.public %>/index.html', dest: '<%= config.dist %>//index.html'},
        {
          expand: true,
              cwd: '<%= config.public %>/images/',
            src: ['**/*.{png,jpg,svg}'],
            dest: '<%= config.dist %>/images/',
            filter: 'isFile'
        }
      ]
    }
  });
};



