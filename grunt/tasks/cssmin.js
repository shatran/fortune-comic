module.exports = function() {
  this.loadNpmTasks("grunt-contrib-cssmin");

  this.config("cssmin", {
    target: {
      files: [{
        expand: true,
        cwd: '<%= config.build %>',
        src: ['**/*.css', '!**/*.min.css'],
        dest: '<%= config.dist %>'
      }]
    }
  });
}
