module.exports = function() {
  this.loadNpmTasks("grunt-contrib-compass");

  this.config("compass", {
    options: {
      relativeAssets: true,
      trace: true,
      importPath: ["node_modules","bower_components"]
    },
    dev: {
      options: {
        sassDir: "<%= config.public %>/styles",
        cssDir: "<%= config.build %>/styles",
        imagesDir: "<%= config.public %>/images",
        javascriptsDir: "<%= config.public %>/scripts",
        fontsDir: "<%= config.public %>/styles/fonts"
      }
    }
  });
};
