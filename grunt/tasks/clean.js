module.exports = function() {
  this.loadNpmTasks("grunt-contrib-clean");

  // Wipe out previous builds and test reporting.
  this.config("clean", {
    build: ['<%= config.build %>'],
    dist: ['<%= config.dist %>']
  });
};

