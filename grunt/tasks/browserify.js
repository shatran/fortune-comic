_ = require('lodash');

var getTransforms = function() {
  return [
    ['babelify', {presets: ["es2015", "react"]}]
  ]
};

var browserifyOptions = {
  paths: ['./<%= config.public %>'],
  extensions: ['.js']
};

var files = {
  '<%= config.build %>/app.js': ['<%= config.public %>/scripts/main.js']
};

module.exports = function() {
  this.loadNpmTasks("grunt-browserify");

  this.config("browserify", {
    options: {},
    dev: {
      files : files,
      options: {
        transform: getTransforms(),
        watch: true,
        browserifyOptions: _.assign({}, browserifyOptions, {debug: true})
      }
    },
    prod: {
      files : files,
      options: {
        transform: getTransforms(),
        watch: false,
        browserifyOptions: browserifyOptions
      }
    }
  });
};


