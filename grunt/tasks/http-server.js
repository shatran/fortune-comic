module.exports = function() {
  this.loadNpmTasks('grunt-http-server');

  this.config("http-server", {

    'dev': {

      // the server root directory
      root: '<%= config.build %>',

      // the server port
      port: 8282,

      // the server host
      host: "0.0.0.0",

      showDir : true,
      autoIndex: true,

      // server default file extension
      ext: "html",

      // run in parallel with other tasks
      runInBackground: true,

      // Tell grunt task to open the browser
      openBrowser : true
    }
  });
};