/* Constants */
import actionTypes from '../constants/action-types.js';

function changeSlotMachineStatus(status) {
  return {
    type: actionTypes.SLOT_MACHINE_STATUS_CHANGE,
    status
  }
}

export function dispatchChangeSlotMachineStatus(status) {
  return (dispatch, getState) => {
    return dispatch(changeSlotMachineStatus(status));
  }
}