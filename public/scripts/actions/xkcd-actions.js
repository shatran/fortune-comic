/* Utils */
import webRequestsAPI from '../utils/web-requests-utils';

/* Constants */
import actionTypes from '../constants/action-types';
import { XKCD_API_URL } from '../constants/xkcd-api-url';

function getComicUrl(comicId) {
  return `${XKCD_API_URL}?num=${comicId}`;
}

function requestXkcdComic(comicId) {
  return {
    type: actionTypes.REQUEST_XKCD_COMIC,
    comicId
  }
}

function receiveXkcdComic(comicData) {
  return {
    type: actionTypes.RECEIVE_XKCD_COMIC,
    comicData
  }
}

function receiveXkcdComicError(error) {
  return {
    type: actionTypes.RECEIVE_XKCD_COMIC_ERROR,
    error
  }
}

export function dispatchXkcdComicRequest(comicId) {
  return function (dispatch) {
    dispatch(requestXkcdComic(comicId));
    return webRequestsAPI.getJson(getComicUrl(comicId))
      .then(function (comicData) {
        dispatch(receiveXkcdComic(comicData[0]));
      }).catch(function (error) {
        dispatch(receiveXkcdComicError(error));
      })
  }
}