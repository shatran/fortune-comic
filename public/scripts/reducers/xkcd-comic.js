/* Constants */
import actionTypes from '../constants/action-types.js';

function xkcdComic(state = {
  id: null,
  title: null,
  imgUrl: null
}, action = null) {
  switch (action.type) {
    case actionTypes.REQUEST_XKCD_COMIC:
      return Object.assign({}, state, {
          id: action.comicId
        }
      );
    case actionTypes.RECEIVE_XKCD_COMIC:
      return Object.assign({}, state, {
          title: action.comicData.title,
          imgUrl: action.comicData.img
        }
      );

    default:
      return state;
  }
}

export default xkcdComic;