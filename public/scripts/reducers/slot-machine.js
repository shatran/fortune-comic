/* Constants */
import actionTypes from '../constants/action-types.js';
import slotMachineOperationalStates from '../constants/slot-machine-states';

function slotMachine(state = {
  status: slotMachineOperationalStates.STATIC,
  result: 0
}, action = null) {
  switch (action.type) {
    case actionTypes.SLOT_MACHINE_STATUS_CHANGE:
      return Object.assign({}, state, {
            status: action.status
          }
      );
    case actionTypes.SLOT_MACHINE_CURRENT_RESULT:
      return Object.assign({}, state, {
            result: action.result
          }
      );
    default:
      return state;
  }
}

export default slotMachine;
