/* Redux */
import { combineReducers } from 'redux';

/* Reducers */
import slotMachine from './slot-machine';
import xkcdComic from './xkcd-comic';

export default combineReducers({ slotMachine, xkcdComic });
