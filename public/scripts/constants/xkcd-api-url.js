/* XKCD api url constants */
const XKCD_API_URL = 'http://xkcd-unofficial-api.herokuapp.com/xkcd';

export {
  XKCD_API_URL
};
