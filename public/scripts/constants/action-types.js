/* XDCK actions constants */
const REQUEST_XKCD_COMIC = 'REQUEST_XKCD_COMIC';
const RECEIVE_XKCD_COMIC = 'RECEIVE_XKCD_COMIC';
const RECEIVE_XKCD_COMIC_ERROR = 'RECEIVE_XKCD_COMIC_ERROR';
/* Slot machine actions constants */
const SLOT_MACHINE_STATUS_CHANGE = 'SLOT_MACHINE_STATUS_CHANGE';
const SLOT_MACHINE_CURRENT_RESULT = 'SLOT_MACHINE_CURRENT_RESULT';

export default {
  REQUEST_XKCD_COMIC,
  RECEIVE_XKCD_COMIC,
  SLOT_MACHINE_STATUS_CHANGE,
  SLOT_MACHINE_CURRENT_RESULT
};