/* Utils */
import fetchPackege from 'whatwg-fetch';


function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    let error = new Error(response.statusText);
    error.response = response;
    throw error
  }
}

function parseJSON(response) {
  return response.json();
}

// API for web requests
const webRequestsAPI = {
  getJson(url) {
    return fetch(url)
      .then(checkStatus)
      .then(parseJSON)
      .then(function (data) {
        return data;
      })
  },

  postJson(url, jsonObj) {
    return fetch(url, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(jsonObj)
    });
  }
};

export default webRequestsAPI
