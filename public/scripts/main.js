import React from 'react';
import ReactDOM from 'react-dom';

/* Redux */
import { Provider } from 'react-redux';
import configureStore from './store/configure-store.js';

/* Components */
import XkcdSlotMain from './containers/xkcd-slot-main.js';

// create the store that handles the app state
const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
      <XkcdSlotMain />
    </Provider>,
    document.getElementById('main')
);
