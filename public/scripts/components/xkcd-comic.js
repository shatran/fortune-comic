import React, { Component, PropTypes } from 'react';

/* constants */
import slotMachineOperationalStates from '../constants/slot-machine-states';

export default class XkcdComic extends Component {

  render() {
    const {
        xkcdComic,
        slotMachineState
        } = this.props;

    return (
        <div className="xkcd-comic">
          {
            slotMachineState === slotMachineOperationalStates.STATIC ? (
                <div>
                  <h3>{xkcdComic.title}</h3>
                  <img src={xkcdComic.imgUrl} />
                </div>
            ) : null
          }
        </div>
    )
  }
};

XkcdComic.propTypes = {
  xkcdComic: PropTypes.shape({
    title: PropTypes.string,
    imgUrl: PropTypes.string
  }),
  slotMachineState: PropTypes.string
};

XkcdComic.defaultProps = {
  xkcdComic: {
    title: '',
    imgUrl: ''
  },
  slotMachineState: slotMachineOperationalStates.STATIC
};
