import React, { Component, PropTypes } from 'react';

/* Utilities */
import classNames from 'classnames';
import moment from 'moment';

/* constants */
import slotMachineOperationalStates from '../constants/slot-machine-states';

export default class SlotMachine extends Component {

  constructor(props) {
    super(props);
    this.state = {
      reels: [
        {
          value: 0,
          speed: 0,
          step: 1.25,
          atTopSpeed: false
        },
        {
          value: 0,
          speed: 0,
          step: 1.5,
          atTopSpeed: false
        },
        {
          value: 0,
          speed: 0,
          step: 1.75,
          atTopSpeed: false
        }
      ]
    };
  }

  componentWillReceiveProps(nextProps) {
    // start operation according to the props indicating the state of the slot
    switch (nextProps.slotMachineState) {
      case slotMachineOperationalStates.SHOULD_START_SPINNING:
        this._startSlotSpin();
        break;
      case slotMachineOperationalStates.SHOULD_STOP_SPINNING:
        this._stopSlotSpin();
        break;
      default:
        break;
    }
  }

  _convertSpeedToAnmiationSpeed(speed) {
    // convert the given speed to animation speed by reducing the speed from the number of speed levels defined.
    // the lower the animation speed value, the faster the animation will occur (except for 0 value which means no animation).
    // if speed is higher then numOfSpeedLevels (it can get higher in some values of 'step') then we are at the max speed.
    return speed >= (this.props.numOfSpeedLevels + this.props.animationTopSpeed) ? this.props.animationTopSpeed : speed == 0 ? 0 : (this.props.numOfSpeedLevels + this.props.animationTopSpeed - speed);
  }

  _getSlotCurrentBackgoundStyles(speed) {
    // convert the given speed to animation speed.
    let animationSpeed = this._convertSpeedToAnmiationSpeed(speed);

    return {
      animation: `animateBackgroundDown ${animationSpeed}s linear infinite`,
      'WebkitAnimation': `animateBackgroundDown ${animationSpeed}s linear infinite`,
      'MozAnimation': `animateBackgroundDown ${animationSpeed}s linear infinite`,
      'Oanimation': `animateBackgroundDown ${animationSpeed}s linear infinite`,
      'MsAnimation': `animateBackgroundDown ${animationSpeed}s linear infinite`
    };
  }

  _startSlotSpin() {
    let startSpinningDate = moment();

    // set an interval to increase the reel speed at a set pace
    this.increaseSpinSpeedInterval = setInterval(()=> {
      let reels = this.state.reels;
      // increase speed for all reels - each according to its step
      for (let reel of reels) {
        if (reel.speed < this.props.numOfSpeedLevels + this.props.animationTopSpeed) {
          reel.speed += reel.step;
        } else {
          reel.atTopSpeed = true;
        }
      }
      // if all reels are at top speed, allow slot to stop
      let allReelsAtTopSpeed = reels.reduce((previousValue, currentReel)=> {
        return previousValue && currentReel.atTopSpeed
      }, reels[0].atTopSpeed);

      if (allReelsAtTopSpeed) {
        this.props.dispatchChangeSlotMachineStatus(slotMachineOperationalStates.SPINNING_AT_TOP_SPEED);

        // after a defined amount of seconds, stop the slot machine anyway
        if (moment().diff(startSpinningDate, 'seconds') === this.props.maximumSpinTimeInSeconds) {
          clearInterval(this.increaseSpinSpeedInterval);
          // start slot stopping process
          this.props.dispatchChangeSlotMachineStatus(slotMachineOperationalStates.SHOULD_STOP_SPINNING);
        }
      }

      // save the new reels state
      this.setState({reels: reels});
    }, 40);

    // change the slot machine state to spinning
    this.props.dispatchChangeSlotMachineStatus(slotMachineOperationalStates.SPINNING);

    // raffle the next reel values
    this.tempValues = this._raffleReelValues();
    this.props.dispatchXkcdComicRequest(this._calcSlotMachineResult(this.tempValues));
  }

  _stopSlotSpin() {
    clearInterval(this.increaseSpinSpeedInterval);

    this.decreaseSpinSpeedInterval = setInterval(()=> {
      let reels = this.state.reels;

      reels.forEach((element, index)=> {
        if (element.speed > this.props.bottomSpeedLimit) {
          // the reels should decelerate in a reverse of their acceleration order
          reels[index].speed -= reels[reels.length - 1 - index].step;
          reels[index].atTopSpeed = false;
        } else {
          // if reel is at minimum speed, stop it and show the current value
          reels[index].speed = 0;
          reels[index].value = this.tempValues[index];
        }
      });
      // if all reels are at zero speed, clear the interval and send a 'slot finished' event
      let totalSpeeds = reels.reduce((previousValue, currentReel)=> {
        return previousValue + currentReel.speed
      }, reels[0].speed);

      if (!totalSpeeds) {
        clearInterval(this.decreaseSpinSpeedInterval);
        // change the slot machine state to spinning
        this.props.dispatchChangeSlotMachineStatus(slotMachineOperationalStates.STATIC);
      }

      // save the new reels state
      this.setState({reels: reels});
    }, 100);
  }

  _raffleReelValues() {
    let reels = this.state.reels,
        reelsValues = [];

    // raffle reel value
    for (let reel of reels) {
      reelsValues.push(this._getRandomInt(0, 9));
    }
    return reelsValues;
  }

  // Returns a random integer between min (inclusive) and max (inclusive)
  _getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  // convert reels values to an integer
  _calcSlotMachineResult(reelsValues) {
    let index,
        multiplier = 1,
        result = 0;

    for (index=reelsValues.length-1; index>=0; index--) {
      result = result + reelsValues[index] * multiplier;
      multiplier *= 10;
    }
    return result;
  }

  render() {
    return (
      <div className="slot-machine">
        <div className="slot-wrapper">
          {
            this.state.reels.map((reel, index)=> {
              return <div key={`reel-${index}`} className={classNames("reel", `reel-number-${reel.value}`,{motion: reel.speed})} style={this._getSlotCurrentBackgoundStyles(reel.speed)}></div>
            })
          }
        </div>
      </div>
    )
  }
}

SlotMachine.propTypes = {
  animationTopSpeed: PropTypes.number,
  numOfSpeedLevels: PropTypes.number,
  bottomSpeedLimit: PropTypes.number,
  maximumSpinTimeInSeconds: PropTypes.number,
  slotMachineState: PropTypes.string
};

SlotMachine.defaultProps = {
  animationTopSpeed: 0.5,
  numOfSpeedLevels: 60,
  bottomSpeedLimit: 45,
  maximumSpinTimeInSeconds: 4,
  slotMachineState: slotMachineOperationalStates.STATIC
};
