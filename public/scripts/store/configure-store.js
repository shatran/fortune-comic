/* Redux */
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers/root-reducer.js';

let store = null;

const middlewares = [
  thunkMiddleware
];

const devFunctions = [
  window.devToolsExtension ? window.devToolsExtension() : f => f // See https://github.com/zalmoxisus/redux-devtools-extension
];

export default function configureStore(initialState) {

  if (!store) {
    if (process.env.NODE_ENV === 'production') {
      store = applyMiddleware(...middlewares)(createStore);
    }
    else {
      store = compose(
        applyMiddleware(...middlewares),
        ...devFunctions
      )(createStore);
    }
  }

  return store(rootReducer, initialState);
}