import React, { Component, PropTypes } from 'react';

/* Sub-Components */
import SlotMachine from '../components/slot-machine';
import XkcdComic from '../components/xkcd-comic';

/* constants */
import slotMachineOperationalStates from '../constants/slot-machine-states';

/* Redux */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { dispatchChangeSlotMachineStatus } from '../actions/slot-actions';
import { dispatchXkcdComicRequest } from '../actions/xkcd-actions';


class XkcdSlotMain extends Component {

  _startSpinningButtonClicked() {
    // change slot machine status to 'shouldStartSpinning'
    this.props.dispatchChangeSlotMachineStatus(slotMachineOperationalStates.SHOULD_START_SPINNING);
  }

  _stopSpinningButtonClicked() {
    // change slot machine status to 'stopped'
    this.props.dispatchChangeSlotMachineStatus(slotMachineOperationalStates.SHOULD_STOP_SPINNING);
  }

  render() {
    const {
        xkcdComic,
        slotMachineState,
        dispatchChangeSlotMachineStatus,
        dispatchXkcdComicRequest
      } = this.props;

    return (
      <div className="container app-container">
        <div className="row">
          <div className="col-xs-offset-1 col-xs-10 text-center">
            <h1>Fortune Comic</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <SlotMachine slotMachineState={slotMachineState.status}
                         dispatchChangeSlotMachineStatus={dispatchChangeSlotMachineStatus}
                         dispatchXkcdComicRequest={dispatchXkcdComicRequest}/>
          </div>
        </div>
        <div className="row">
          <div className="col-xs-offset-1 col-xs-10 text-center">
            { (slotMachineState.status === slotMachineOperationalStates.STATIC) ? (
                <button className="spin-btn btn btn3d btn-lg btn-success"
                        disabled={slotMachineState.status !== slotMachineOperationalStates.STATIC}
                        onClick={this._startSpinningButtonClicked.bind(this)}>Spin</button>
              ) : (
                <button className="spin-btn btn btn3d btn-lg btn-danger"
                        disabled={slotMachineState.status === slotMachineOperationalStates.SPINNING ||
                                  slotMachineState.status === slotMachineOperationalStates.SHOULD_STOP_SPINNING}
                        onClick={this._stopSpinningButtonClicked.bind(this)}>Stop</button>
              )
            }
          </div>
        </div>
        <div className="row">
          <div className="col-xs-offset-1 col-xs-10 text-center">
            <XkcdComic xkcdComic={xkcdComic}
                       slotMachineState={slotMachineState.status}/>
          </div>
        </div>
      </div>
    );
  }
}

// Which part of the Redux global state does our component want to receive as props
function mapStateToProps(state) {
  const { xkcdComic, slotMachine } = state;
  return {
    xkcdComic,
    slotMachineState: slotMachine
  };
}

// Which action creators does it want to receive by props
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    dispatchChangeSlotMachineStatus,
    dispatchXkcdComicRequest
  }, dispatch);
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(XkcdSlotMain);
