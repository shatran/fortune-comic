module.exports = function(grunt) {
  config = {
    public: "public",
    dist: "dist",
    build: "build"
  };

  this.initConfig({ config: config });

  // Load task configurations.
  this.loadTasks("grunt/tasks");

  this.registerTask("dist", [
    "clean",
    "browserify:prod",
    "compass",
    "uglify:js",
    "cssmin",
    "copy:dist",
    "clean:build"
  ]);

  this.registerTask("default", [
    "clean",
    "browserify:dev",
    "compass",
    "copy",
    "http-server:dev",
    "watch"
  ]);
};


